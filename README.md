This repo contains installation instructions for the Linux Users' Group's Discord-Matrix bridge bot.

Code: https://github.com/matrix-org/matrix-appservice-discord

# Prerequisites

- A Synapse Matrix homeserver running and accessible with URL `lug.ncsu.edu`. (setup instructions are at https://gitlab.com/ncsulug/matrix-homeserver)
- git
- Docker and Docker Compose v2

# Installation and Configuration

1. Set up and build the Docker image:
    ```bash
    git clone https://github.com/matrix-org/matrix-appservice-discord.git
    docker compose build
    ```
1. Generate and edit *config.yaml*: `./gen-config`
    - `bridge.domain: lug.ncsu.edu`
    - `homeserverUrl: "https://lug.ncsu.edu"`
    - *Optional:* set `adminMxid`
    - *Recommended:* `disableJoinLeaveNotifications: true`
1. Create a Discord application at https://discord.com/developers/applications. Create a bot user and give it all Privileged Gateway Intents.
1. Copy the application ID and bot token and add them to `config.yaml` as `clientID` and `botToken`, respectively.
1. Generate *discord-registration.yaml*: `./gen-registration`
1. Copy *discord-registration.yaml* to the volume used by the Synapse server, which should be at `/var/lib/docker/volumes/synapse-data/_data` if it's on Docker. Edit *homeserver.yaml* in the same directory to add the bridge as an appservice:
    ```yaml
    app_service_config_files:
      - "discord-registration.yaml"
    ```
    Restart Synapse: `docker restart synapse`.
1. Run `./bridge-cli addbot` to generate a Discord authorization link. Visit it to add the bot to a server. You must be an admin with sufficient permissions to grant the bot all the capabilities you see on the page.
1. Start the bot: `docker compose up` (add `-d` to run it detached, i.e. in the background)
    
# Operation

- Create a room in your Matrix server, make it public, and invite the bot to it. The bot's tag is `@_discord_bot:lug.ncsu.edu`.
- Once the bot joins the room, add yourself (through your Matrix client's UI) to all rooms you would like to bridge. The tag for a given room bridged to the equivalent Discord channel is `#_discord_SERVERID_CHANNELID`. To get these IDs, visit the channel in the browser version of Discord. The URL will look like "https://discord.com/channels/SERVERID/CHANNELID".
- To get admin permissions in a room, get its internal room ID (in Element, this is in **Room info** -> **Room settings** -> **Advanced**) and run `./bridge-cli adminme -m 'ROOM_ID' -u '@YOUR_TAG:lug.ncsu.edu' -p '100'`.
