FROM node:18-slim

COPY ./matrix-appservice-discord /
ENV YARN_CACHE_FOLDER=/root/.yarn
RUN --mount=type=cache,target=/root/.yarn \
    yarn
RUN yarn build

EXPOSE 9005
VOLUME /data

ENV CONFIG=/data/config.yaml
ENV REG=/data/discord-registration.yaml
CMD node /build/src/discordas.js --config $CONFIG --file $REG
